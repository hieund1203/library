package com.mycompany.example.controller;

import com.mycompany.example.model.user.Librarian;

public abstract class Controller {
	public int validate(Object obj) {
		if(obj instanceof Librarian) {
			return 1;
		}
		return 2;
	}
}
