package com.mycompany.example.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import com.mycompany.example.Database;
import com.mycompany.example.Example;
import com.mycompany.example.model.user.Borrower;
import com.mycompany.example.model.user.Librarian;
import com.mycompany.example.model.user.User;

public class UserController {
	
	public void addUser(User user) {
		user.addUser();
	}
	public void removeUserById(String id) {
		Example.loggedUser.removeUserById(id);
		System.out.println("User has been deleted successfully.  \n\n");
		
	}
	
	
	public void removeUserByName(String name) {
		Example.loggedUser.removeUserByName(name);
		System.out.println("User has been deleted successfully. \n\n");
	}
	
	
}
