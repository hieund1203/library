package com.mycompany.example.model.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import com.mycompany.example.Database;

public class Librarian extends User{
	
	public Librarian(int id, String name, String password, String phone, String address) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.address = address;
		this.phone = phone;
		this.role_id = 1;
	}
	public void addUser() {
		String sql = "INSERT INTO `user`(`name`, `password`, `phone`, `address`, `role_id`) VALUES (?, ?, ?, ?, ?)";
		try {
			Connection connection = Database.getDatabase().connection;
			PreparedStatement preparedStmt = connection.prepareStatement(sql);
			preparedStmt.setString(1, this.name);
			preparedStmt.setString(2, this.password);
			preparedStmt.setString(3, this.phone);
			preparedStmt.setString(4, this.address);
			preparedStmt.setInt(5, this.role_id);
			preparedStmt.execute();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("User added successfully! \n\n");
	}
	public void removeUserById(String id) {
		String sql = "DELETE FROM `user` WHERE `id` = ?";
		try {
			Connection connection = Database.getDatabase().connection;
			PreparedStatement preparedStmt = connection.prepareStatement(sql);
			preparedStmt.setInt(1, Integer.parseInt(id));
			preparedStmt.execute();
			connection.close();
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
public void removeUserByName(String name) {
	String sql = "DELETE FROM `user` WHERE `name` = ?";
	try {
		Connection connection = Database.getDatabase().connection;
		PreparedStatement preparedStmt = connection.prepareStatement(sql);
		preparedStmt.setString(1, name);
		preparedStmt.execute();
		connection.close();
	}
	catch(Exception e) {
		System.out.println(e.getMessage());
	}
	}
	
	
	
}
