package com.mycompany.example.model.user;

public class Borrower extends User{
	public Borrower(int id, String name, String password, String phone, String address) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.address = address;
		this.phone = phone;
		this.role_id = 2;
	}
	
}
