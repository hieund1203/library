package com.mycompany.example.model.login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mycompany.example.Database;
import com.mycompany.example.model.Model;
import com.mycompany.example.model.user.Borrower;
import com.mycompany.example.model.user.Librarian;
import com.mycompany.example.model.user.User;

public class Login{
	
	public String username, password;
	
	public User getUser(String name, String password) {
		User user = null;
		try {
			Connection connection = Database.getDatabase().connection;
			Statement stmt = connection.createStatement();  
			ResultSet rs = stmt.executeQuery("SELECT * FROM `user` WHERE name = '" + name + "'"); 
			while(rs.next())  {
				if(password.equals(rs.getString(3))) {
					//check user role
					if(rs.getInt(6) == 1 ) {
						user = new Librarian(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
					}
					else {
						user = new Borrower(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
					}
				}
			
			}
		} catch(Exception e) {
			
			System.out.println(e.getMessage());
		}
		return user;
	}
}
