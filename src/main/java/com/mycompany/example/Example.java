package com.mycompany.example;

import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

import com.mycompany.example.controller.LoginController;
import com.mycompany.example.controller.UserController;
import com.mycompany.example.model.user.Borrower;
import com.mycompany.example.model.user.Librarian;
import com.mycompany.example.model.user.User;

/**
 *
 * @author HieuND2
 */

public class Example {

	static int selection;
	public static User loggedUser = null;
	
    public static void main(String[] args) throws RuntimeException {
        
		Scanner input = new Scanner(System.in);
        while(true) {
        	System.out.println("Login");
        	System.out.print("Enter username: ");
        	String name = input.nextLine();
        	System.out.print("Enter password: ");
        	String password = input.nextLine();
        	
        	LoginController loginController = new LoginController();
        	loginController.doLogin(name, password);
        	if(loggedUser != null) {
        		UserController userController = new UserController();
        		System.out.println("Hi: " + loggedUser.name);
        		if(loggedUser instanceof Librarian) {
        			while(true) {
        				System.out.println("1. Personal information");
            			System.out.println("2. Add user");
            			System.out.println("3. Remove user");
            			System.out.println("4. List of books");
            			System.out.println("5. Add book");
            			System.out.println("6. Remove book");
            			System.out.println("7. List of overdue return book");
            			System.out.print("Enter your selection:");
            			selection = Integer.parseInt(input.nextLine());
            			
            			switch(selection) {
	            			case 1: {
	            				System.out.println("PERSONAL INFORMATION:");
	            				System.out.println("Name: " + loggedUser.name + "\nID: " + loggedUser.id);
	            				break;
	            			}
	            			case 2: {
	            				System.out.println("ADD USER:");
	            				System.out.print("Enter name: ");
	            				String userName = input.nextLine();
	            				System.out.print("Enter password: ");
	            				String userPassword = input.nextLine();
	            				System.out.print("Enter role (1: librarian, 2: borrower): ");
	            				int userRole = Integer.parseInt(input.nextLine());
	            				System.out.print("Enter phone number: ");
	            				String userPhone = input.nextLine();
	            				System.out.print("Enter address: ");
	            				String userAddress = input.nextLine();
	            				switch(userRole) {
		            				case 1: {
		            					new UserController().addUser(new Librarian(0, userName, userPassword, userPhone, userAddress));
		            					break;
		            				}
		            				case 2: {
		            					new UserController().addUser(new Borrower(0, userName, userPassword, userPhone, userAddress));
		            				}
	            				}
	            				
	            				break;
	            			}
	       
	            			case 3 : {
	            				System.out.println("REMOVE USER:");
            					System.out.println("1. Search by ID");
            					System.out.println("2. Search by name");
            					
            					System.out.print("Enter your selection: ");
            					selection = Integer.parseInt(input.nextLine());
            					switch(selection) {
		            				case 1: {
		            					System.out.print("Enter user ID to remove: ");
		            					String uid = input.nextLine();			       
		            					UserController usercontroller = new UserController();
		            					usercontroller.removeUserById(uid);			            				
		            					break;
		            					
		            				}
		            				case 2: {
		            					System.out.print("Enter user name to remove: ");
		            					String uid = input.nextLine();			       
		            					UserController usercontroller = new UserController();
		            					usercontroller.removeUserByName(uid);	
		            					break;
		            				}
		            				default : {
			            				System.out.println("Invalid Choice Entered \n");
			            			}
		            				
	            				}
            					break;
	            			}
	            			
	            			default : {
	            				System.out.println("Invalid Choice Entered \n");
	            			}
            			}
        			}
        		}
        		else if(loggedUser instanceof Borrower) {
        			System.out.println("1. Personal information");
        			System.out.println("2. List of books");
        			System.out.println("3. Borrowed history");
        			System.out.println("4. New borrow");
        			System.out.print("Enter your selection:");
        			int selection = Integer.parseInt(input.nextLine());
        		}
        	}
        	else {
        		System.out.println("Wrong password");
        	}
        	break;
        }
    }
}
